# Operaciones `commit` y `rollback` en Python.

### Estos métodos se utilizan para realizar transacciones de base de datos.


1. Método **commit() :** Se utiliza para asegurarse que los cambios realizados 
en la base de datos sean coherentes. Básicamente realiza una confirmación de los cambios hechos.


2. Método **rolback() :** Se utiliza para revertir los últimos cambios  realizados en la base de datos.
Se puede usar el método **rollback()** para recuperar los datos originales que se cambiaron a través del método **commit()**.
