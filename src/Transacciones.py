import psycopg2 as bd
from decouple import config


conexion = bd.connect(
    user=config('user'),
    password=config('password'),
    host=config('host'),
    port=config('port'),
    database=config('database')
)

try:
    conexion.autocommit = False
    cursor = conexion.cursor()
    query = 'INSERT INTO persona(nombre,apellido,edad,email)' \
            'VALUES(%s,%s,%s,%s)'
    valor = ('marcela', 'lizama', 1, 'm.lizama@mail.com')
    cursor.execute(query, valor)
    update = 'UPDATE persona SET nombre=%s WHERE id_persona=%s'
    dato = ('mario', 3)
    cursor.execute(update, dato)
    conexion.commit()
    print("termina la conexion")
except Exception as e:
    conexion.rollback()
    print(f'ocurrio un error se hizo rollback de la transacción: {e}')
finally:
    conexion.close()
